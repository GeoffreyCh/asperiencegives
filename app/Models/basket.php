<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state',
        'id_user',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_user' => 'integer',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'basket_product', 'basket_id', 'product_id')->withPivot(['quantity', 'dateAddProduct']);

    }

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_basket', 'basket_id', 'user_id');
    }
}
