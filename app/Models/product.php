<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'brand',
        'reference',
        'active',
        'stock',
        'previsionalStock',
        'image',
        'description',
        'id_category',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_category' => 'integer',
    ];

    public function baskets()
    {
        return $this->belongsToMany(Basket::class, 'basket_product', 'product_id', 'basket_id')->withPivot(['quantity', 'dateAddProduct']);
    }

    public function Category()
    {
        return $this->belongsTo(Category::class);

    }
}
