<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\categoryStoreRequest;
use App\Http\Requests\categoryUpdateRequest;

class categoryController extends Controller
{

    public function index(Request $request)
    {
        $categories = Category::all();

        return view('category.index', compact('categories'));
    }


    public function create(Request $request)
    {
        return view('category.create');
    }


    public function store(categoryStoreRequest $request)
    {
        $category = Category::create($request->validated());

        $request->session()->flash('category.id', $category->id);

        return redirect()->route('category.index');
    }


    public function show(Request $request, category $category)
    {

        $products = Product::where('id_category', $category->id)->get();
        $categories = Category::all();

        if(auth()->check()){
            $user = Auth::user();
            $baskets = $user->basket;

            foreach ($baskets as $value){
                if($value->state == 'draft'){
                    $basket = $value;
                }
            }

            $productsBasket = $basket->products;
            $nbProductBasket = 0;

            foreach($productsBasket as $productBasket){
                $nbProductBasket += $productBasket->pivot->quantity;
            };

            return view('product.index', compact(['products', 'basket', 'categories', 'nbProductBasket']));
        } else {

            return view('product.index', compact(['products', 'categories']));
        }

        return view('category.show', ['products'=>$products, 'category'=>$category]);
    }


    public function edit(Request $request, category $category)
    {
        return view('category.edit', compact('category'));
    }


    public function update(categoryUpdateRequest $request, category $category)
    {
        $category->update($request->validated());

        $request->session()->flash('category.id', $category->id);

        return redirect()->route('category.index');
    }


    public function destroy(Request $request, category $category)
    {
        $category->delete();

        return redirect()->route('category.index');
    }
}
