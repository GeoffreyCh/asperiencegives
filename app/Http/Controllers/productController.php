<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Basket;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Spatie\SimpleExcel\SimpleExcelReader;
use Spatie\SimpleExcel\SimpleExcelWriter;
use App\Http\Requests\productStoreRequest;
use App\Http\Requests\productUpdateRequest;

class productController extends Controller
{

    public function index(Request $request)
    {
        $products = Product::all();
        $categories = Category::all();

        if(auth()->check()){
            $user = Auth::user();
            $baskets = $user->basket;

            foreach ($baskets as $value){
                if($value->state == 'draft'){
                    $basket = $value;
                }
            }

            $productsBasket = $basket->products;
            $nbProductBasket = 0;

            foreach($productsBasket as $productBasket){
                $nbProductBasket += $productBasket->pivot->quantity;
            };

            return view('product.index', compact(['products', 'basket', 'categories', 'nbProductBasket']));
        } else {

            return view('product.index', compact(['products', 'categories']));
        }
    }


    public function create()
    {

    }

    //enregistrement produit via formulaire
    public function store()
    {

    }


    public function show(Request $request, product $product)
    {

        return view('product.show', compact('product'));
    }


    public function edit(Request $request, product $product)
    {

    }


    public function update(Request $request, product $product)
    {

    }


    public function destroy(Request $request, product $product)
    {

    }


    public function addProductToBasket(product $product)
    {
        $user = Auth::user();
        $baskets = $user->basket;

        foreach ($baskets as $value){
            if($value->state == 'draft'){
                $basket = $value;
            }
        }

        $quantity = request('quantity');

        if($product->previsionalStock >= $quantity && $quantity >= 1){
            $product->previsionalStock -= $quantity;
            $product->save();

            $itemBasket = $basket->products;

            foreach($itemBasket as $item){
                if($product->id == $item->pivot->product_id){
                    $item->pivot->quantity += $quantity;
                    $item->pivot->save();

                    return redirect()->route('product.index');
                };
            };

            $basket->products()->attach($product);
            $basket->products()->updateExistingPivot($product, ['quantity'=>$quantity]);
            $basket->products()->updateExistingPivot($product, ['dateAddProduct'=>Date::now()]);

            return redirect()->route('product.index');

        } else {
            return redirect()->route('product.index')->with('message', 'Stock insuffisant !');
        }
    }


    public function delProductToBasket(product $product)
    {

        $user = Auth::user();
        $baskets = $user->basket;

        foreach ($baskets as $value){
            if($value->state == 'draft'){
                $basket = $value;
            }
        }
        $quantity = request('quantity');

        $product->previsionalStock += $quantity;
        $product->save();

        $itemBasket = $basket->products;
        foreach($itemBasket as $item){
            if($product->id == $item->pivot->product_id){
                if($quantity == $item->pivot->quantity){
                    $basket->products()->detach($product);

                    return redirect()->route('basket.show', ['basket'=>$basket]);
                } else {
                    $item->pivot->quantity -= $quantity;
                    $item->pivot->save();

                    return redirect()->route('basket.show', ['basket'=>$basket]);
                };
            };
        }
    }

}
