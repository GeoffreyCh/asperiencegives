<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Basket;
use App\Models\User;

class PostController extends Controller
{

    public function index()
    {
        return('coucou');
    }


    public function create()
    {
        $categories = Category::all();

        return view('product.create', ['categories'=>$categories]);
    }


    public function store(Request $request)
    {
        $products = Product::all();
        $reference = request('reference');
        $stock = request('stock');

        foreach($products as $product){
            if($product->reference === $reference){
                $product->stock += $stock;
                $product->previsionalStock += $stock;

                $product->save();

                return redirect()->route('product.index');
            };
        };

        $all_params = request([
            'name',
            'brand',
            'reference',
            'stock',
            'image',
            'description',
            'id_category'
        ]);

        $product = Product::create($all_params);
        $product->previsionalStock = $stock;
        $product->save();

        return redirect()->route('product.index');
    }


    public function show($id)
    {
        //
    }


    public function edit(product $product)
    {
        $categories = Category::all();

        return view('product.edit', compact(['product', 'categories']));
    }


    public function update(request $request, product $product)
    {
        $product->name = $request->name;
        $product->brand = $request->brand;
        $product->reference = $request->reference;
        $product->stock = $request->stock;
        $product->image = $request->Image;
        $product->description = $request->description;
        $product->id_category = $request->id_category;

        $product->save();

        return redirect()->route('product.index');
    }


    public function destroy($id)
    {
        $product->delete();

        return redirect()->route('product.index');
    }


    public function storeViaCsv(request $request)
    {

        $csv = array_map('str_getcsv', file($request->file('file')->path()));
        $labels = $csv[0];
        unset($csv[0]);
        $datas = [];

        foreach($csv as $line) {
            $hardware = [];
            for($i=0; $i<count($line); $i++) {
                $hardware[$labels[$i]] = $line[$i];
            }
            array_push($datas, $hardware);
        }

        foreach($datas as $data) {
            $category_exist = Category::where('name', $data['category'])->first();
            if(!$category_exist) {
                $category = Category::create([
                    'name' => $data['category']
                ]);
            } else {
                $category = $category_exist;
            }


            $product = Product::where('reference', $data['reference'])->first();
            if($product != null && $data['reference'] == $product->reference) {
                $product->stock += $data['stock'];
                $product->previsionalStock += $data['stock'];
                $product->save();
            }
            else {
                Product::create([
                    'reference' => $data['reference'],
                    'name' => $data['name'],
                    'brand' => $data['brand'],
                    'id_category' => $category->id,
                    'description' => $data['description'],
                    'image' => $data['image'],
                    'stock' => $data['stock'],
                    'previsionalStock' => $data['stock']
                ]);
            }
        }

        return redirect()->route('product.index');

    }
}
