<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Basket;
use App\Models\Product;
use App\Models\Category;
use App\Mail\validateMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\basketStoreRequest;
use App\Http\Requests\basketUpdateRequest;

class basketController extends Controller
{


    public function index(){


    }

    public function show(Request $request, basket $basket)
    {
        $products = $basket->products;
        $nbProductBasket = 0;
        $categories = Category::all();

        foreach($products as $product){
            $nbProductBasket += $product->pivot->quantity;
        };

        return view('basket.show', compact(['basket', 'products', 'nbProductBasket', 'categories']));
    }


    public function tryValidateBasket(basket $basket)
    {

        return view('basket.reserved', ['basket'=>$basket]);
    }


    public function validateBasket(basket $basket)
    {
        $date = request('date');
        $time = request('time');

        $basket->dateValidate = Date::now();
        $basket->dateRecupProduct = $date;
        $basket->hourRecupProduct = $time;
        $basket->state = 'reserved';
        $basket->save();

        $user = Auth::user();
        $newBasket = Basket::create([
            'state'=>'draft'
        ]);
        $newBasket->user()->attach($user);
        $newBasket->save();

        return redirect()->route('product.index');
    }


    public function acceptedBasket(basket $basket)
    {
        $basket->state = "validated";
        $basket->save();

        $users = $basket->user;
        $user = $users[0];

        Mail::to($user->email)->send(new validateMail($basket));

        return redirect()->route('admin.basket.historicBasketAdmin');
    }


    public function canceledBasket(basket $basket)
    {
        $basket->state = "canceled";
        $basket->save();

        $products = Product::all();
        $productsBasket = $basket->products;

        foreach($productsBasket as $productBasket){
            foreach($products as $product){
                if($productBasket->reference == $product->reference){
                    $product->previsionalStock += $productBasket->pivot->quantity;
                    $product->save();
                }
            }
        }

        return redirect()->route('product.index');
    }


    public function seeReservation()
    {
        $baskets = Basket::where('state', 'reserved')->get();

        $users = User::all();

        return view('basket.index', compact(['baskets', 'users']));
    }


    public function seeValidateBasket()
    {
        $baskets = Basket::where('state', 'validated')->get();

        $users = User::all();

        return view('basket.validated', compact(['baskets', 'users']));
    }


    public function finishedBasket(basket $basket)
    {
        $basket->state = "finished";
        $basket->save();

        $products = Product::all();
        $productsBasket = $basket->products;

        foreach($productsBasket as $productBasket){
            foreach($products as $product){
                if($productBasket->reference == $product->reference){
                    $product->stock -= $productBasket->pivot->quantity;
                    $product->save();
                }
            }
        }

        return redirect()->route('admin.basket.historicBasketAdmin');
    }


    public function historic()
    {
        $categories = Category::all();

        if(auth()->check()){
            $baskets = Auth::user()->basket;
            $user = Auth::user();

            foreach ($baskets as $value){
                if($value->state == 'draft'){
                    $basket = $value;
                }
            }

            $productsBasket = $basket->products;
            $nbProductBasket = 0;

            foreach($productsBasket as $productBasket){
                $nbProductBasket += $productBasket->pivot->quantity;
            };

            return view('basket.historic', compact(['baskets', 'basket', 'categories', 'nbProductBasket']));
        }else{
            return redirect()->route('product.index');
        }

    }


    public function historicBasketAdmin()
    {
        $baskets = Basket::all();
        $categories = Category::all();
        $state = 'all';

        return view('basket.adminHistoric', compact('baskets', 'categories', 'state'));
    }


    public function filterHistoricAdmin($state)
    {

        $baskets = Basket::where('state', $state)->get();
        $categories = Category::all();

        return view('basket.adminHistoric', compact('baskets', 'categories', 'state'));
    }
}
