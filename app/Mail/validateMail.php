<?php

namespace App\Mail;

use App\Models\basket;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Contracts\Queue\ShouldQueue;

class validateMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $basket;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(basket $basket)
    {
        $this->basket = $basket;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            from: new Address('gc.geoffrey.c@gmail.com'),
            subject: 'Validation de commande',
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'emails.validateBasket',
            with: [
                'products'=>$this->basket->products,
                'basket'=>$this->basket,
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
