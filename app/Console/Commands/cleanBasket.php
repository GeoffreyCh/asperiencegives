<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Basket;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Date;

class cleanBasket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'basket:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Si le panier est en draft et n\'a pas été réservé après une semaine il est vidé';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $baskets = Basket::all();

        foreach ($baskets as $basket) {
            if($basket->state === 'draft'){
                $products = $basket->products;

                foreach ($products as $product) {

                    $diff = date_diff(Carbon::now(), Carbon::createFromFormat('Y-m-d', $product->pivot->dateAddProduct));
                    $diffDate = (int)$diff->format("%a");

                    if($diffDate >= 7){
                        $product->previsionalStock += $product->pivot->quantity;
                        $basket->products()->detach($product);
                        $product->save();
                    }
                }
            }
        }
    }
}
