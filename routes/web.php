<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');



Route::resource('user', App\Http\Controllers\UserController::class);



Route::get('basket/reserved', [App\Http\Controllers\basketController::class, 'seeReservation'])->name('basket.reservation');

Route::get('basket/validated', [App\Http\Controllers\basketController::class, 'seeValidateBasket'])->name('basket.validated');

Route::get('basket/historic', [App\Http\Controllers\basketController::class, 'historic'])->name('basket.historic');

Route::resource('basket', App\Http\Controllers\basketController::class);

Route::get('basket/tryValidate/{basket}', [App\Http\Controllers\basketController::class, 'tryValidateBasket'])->name('basket.tryValidate');

Route::post('basket/validate/{basket}', [App\Http\Controllers\basketController::class, 'validateBasket'])->name('basket.validate');

Route::post('basket/accepted/{basket}', [App\Http\Controllers\basketController::class, 'acceptedBasket'])->name('basket.accepted');

Route::post('basket/finished/{basket}', [App\Http\Controllers\basketController::class, 'finishedBasket'])->name('basket.finished');

Route::get('basket/canceled/{basket}', [App\Http\Controllers\basketController::class, 'canceledBasket'])->name('basket.canceled');


Route::resource('product', App\Http\Controllers\productController::class);

Route::post('product/addProduct/{product}', [App\Http\Controllers\productController::class, 'addProductToBasket'])->name('product.addProduct');

Route::post('product/delProduct/{product}', [App\Http\Controllers\productController::class, 'delProductToBasket'])->name('product.delProduct');



Route::resource('category', App\Http\Controllers\categoryController::class);


require __DIR__.'/auth.php';
