<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\product;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'brand' => $this->faker->word,
            'reference' => $this->faker->ean8,
            'active' => 1,
            'stock' => $this->faker->numberBetween(1,10),
            'previsionalStock' => $this->faker->numberBetween(1,5),
            'image' => $this->faker->imageUrl($width = 640, $height = 480),
            'description' => $this->faker->realText($maxNbChars = 100),
            'id_category' => $this->faker->numberBetween(1,5),
        ];
    }
}
