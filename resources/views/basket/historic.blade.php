@vite(['resources/scss/app.scss', 'resources/js/app.js'])

@include('components.perso.header')

<div class="bodyHistorique">
    <div class="historique">
        <h1><o>V</o>os Commandes</h1>
        <div class="listeHisto">
            <h2><o>C</o>ommande en cours de validation :</h2>
            @foreach ($baskets as $basket)
                @if ($basket->state == 'reserved')
                    <div class="headTable">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Brand</th>
                                    <th>Reference</th>
                                    <th>Quantité</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="bodyTable">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach ( $basket->products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>
                                            @foreach ($categories as $category)
                                                @if ($product->id_category === $category->id)
                                                    {{$category->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$product->pivot->quantity}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                @endif
            @endforeach
            <h2><o>C</o>ommande validée :</h2>
            @foreach ($baskets as $basket)
                @if ($basket->state == 'validated')

                    <div class="headTable">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Brand</th>
                                    <th>Reference</th>
                                    <th>Quantité</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="bodyTable">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach ( $basket->products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>
                                            @foreach ($categories as $category)
                                                @if ($product->id_category === $category->id)
                                                    {{$category->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$product->pivot->quantity}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <p>Retrait prévu pour le {{$basket->dateRecupProduct}} à {{$basket->hourRecupProduct}}</p>
                    <br>
                    <br>
                @endif
            @endforeach
            <h2><o>C</o>ommande annulée :</h2>
            @foreach ($baskets as $basket)
                @if ($basket->state == 'canceled')
                    <div class="headTable">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Brand</th>
                                    <th>Reference</th>
                                    <th>Quantité</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="bodyTable">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach ( $basket->products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>
                                            @foreach ($categories as $category)
                                                @if ($product->id_category === $category->id)
                                                    {{$category->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$product->pivot->quantity}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                @endif
            @endforeach
            <h2><o>C</o>ommande terminée :</h2>
            @foreach ($baskets as $basket)
                @if ($basket->state == 'finished')
                    <div class="headTable">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Brand</th>
                                    <th>Reference</th>
                                    <th>Quantité</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="bodyTable">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach ( $basket->products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>
                                            @foreach ($categories as $category)
                                                @if ($product->id_category === $category->id)
                                                    {{$category->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$product->pivot->quantity}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <p>Commande récupéré le {{$basket->dateRecupProduct}} à {{$basket->hourRecupProduct}}</p>
                    <br>
                    <br>
                @endif
            @endforeach
        </div>
    </div>
</div>

@include('components.perso.footer')
