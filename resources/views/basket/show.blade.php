@vite(['resources/scss/app.scss', 'resources/js/app.js'])

@include('components.perso.header')

<div class="bodyProduct">
    <div class="products">
        <h2><o>P</o>roduits dans votre panier</h2>
        <p class="notifyBasket">
            {{-- * Les produits ajoutées seront supprimés de votre panier s'il n'est pas validé après 1 semaine. --}}
            * Tant que votre panier n'est pas validé, un produit ajouté y sera conservé 1 semaine.
        </p>
        <div class="headTable">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Marque</th>
                        <th>Catégorie</th>
                        <th>Qté</th>
                        <th>Supprimer</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="bodyTable">
            <table cellpadding="0" cellspacing="0">
                <tbody>
                    @foreach ( $products as $product)
                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->brand }}</td>
                            <td>
                                @foreach ($categories as $category)
                                    @if ($product->id_category === $category->id)
                                        {{$category->name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{$product->pivot->quantity}}</td>
                            <td>
                                <form action="{{ route('product.delProduct', ['product'=>$product]) }}" method="POST">
                                    @csrf
                                    <input type="number" name="quantity" max="{{ $product->pivot->quantity }}" min="1">
                                    <br>
                                    <button type="submit" class="btn btn-primary"><p class="delProductBasket">×</p></button>
                                    <br>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="validateBasket">
                <form method="POST" action="{{ route('basket.validate', ['basket'=>$basket]) }}">
                    @csrf
                    <label>Date et Heure de retrait :</label>
                    <input type="date" name="date" required min="2022-10-15">
                    <input type="time" name="time" required>
                    <button type="submit">Valider</button>
                </form>
            </div>
        </div>
    </div>
</div>
@include('components.perso.footer')


