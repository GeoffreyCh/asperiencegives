@vite(['resources/scss/app.scss', 'resources/js/app.js'])

@include('components.perso.header')

<div class="bodyHistorique">
    <div class="categoriesCommand">
        <h2 class="titleListCategory"><o>C</o>atégories</h2>
        <div class="category">

            <a href="{{ route('admin.basket.historicBasketAdmin') }}" class="btnValidated" type="submit">¤ Toutes les commandes</a>

            <form action="{{ route('admin.basket.filterHistoricAdmin', ['state'=>'reserved']) }}" method="POST">
                @csrf
                <button class="btnValidated" type="submit">¤ Commande Réservées</button>
            </form>
            <form action="{{ route('admin.basket.filterHistoricAdmin', ['state'=>'validated']) }}" method="POST">
                @csrf
                <button class="btnValidated" type="submit">¤ Commande Validées</button>
            </form>
            <form action="{{ route('admin.basket.filterHistoricAdmin', ['state'=>'canceled']) }}" method="POST">
                @csrf
                <button class="btnValidated" type="submit">¤ Commande Annulées</button>
            </form>
            <form action="{{ route('admin.basket.filterHistoricAdmin', ['state'=>'finished']) }}" method="POST">
                @csrf
                <button class="btnValidated" type="submit">¤ Commande Terminées</button>
            </form>
        </div>
    </div>
    <div class="historique">
        <h1><o>L</o>iste des Commandes</h1>
        <div class="listeHisto">
            @if ($state === 'reserved' || $state === 'all')
                <h2><o>C</o>ommandes en cours de validation :</h2>
            @endif

            @foreach ($baskets as $basket)
                @if ($basket->state == 'reserved')
                    <p>Panier de {{ $basket->user[0]->name }}</p>
                    <p>Email : {{ $basket->user[0]->email }}</p>
                    <div class="headTable">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Brand</th>
                                    <th>Reference</th>
                                    <th>Quantité</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="bodyTable">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach ( $basket->products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>
                                            @foreach ($categories as $category)
                                                @if ($product->id_category === $category->id)
                                                    {{$category->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$product->pivot->quantity}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <form action="{{ route('basket.accepted', ['basket'=>$basket]) }}" method="POST">
                        @csrf
                        <button class="btnValidated" type="submit">Valider la commande</button>
                    </form>
                    <br>
                    <br>
                @endif
            @endforeach
            @if ($state === 'validated' || $state === 'all')
                <h2><o>C</o>ommandes validées :</h2>
            @endif
            @foreach ($baskets as $basket)
                @if ($basket->state == 'validated')
                    <p>Panier de {{ $basket->user[0]->name }}</p>
                    <p>Email : {{ $basket->user[0]->email }}</p>
                    <div class="headTable">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Brand</th>
                                    <th>Reference</th>
                                    <th>Quantité</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="bodyTable">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach ( $basket->products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>
                                            @foreach ($categories as $category)
                                                @if ($product->id_category === $category->id)
                                                    {{$category->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$product->pivot->quantity}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <p>Retrait prévu pour le {{$basket->dateRecupProduct}} à {{$basket->hourRecupProduct}}</p>
                    <form action="{{ route('basket.finished', ['basket'=>$basket]) }}" method="POST">
                        @csrf
                        <button class="btnValidated" type="submit">Commande terminée</button>
                    </form>
                    <br>
                    <br>
                @endif
            @endforeach
            @if ($state === 'canceled' || $state === 'all')
                <h2><o>C</o>ommandes annulées :</h2>
            @endif
            @foreach ($baskets as $basket)
                @if ($basket->state == 'canceled')
                    <p>Panier de {{ $basket->user[0]->name }}</p>
                    <p>Email : {{ $basket->user[0]->email }}</p>
                    <div class="headTable">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Brand</th>
                                    <th>Reference</th>
                                    <th>Quantité</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="bodyTable">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach ( $basket->products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>
                                            @foreach ($categories as $category)
                                                @if ($product->id_category === $category->id)
                                                    {{$category->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$product->pivot->quantity}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                @endif
            @endforeach
            @if ($state === 'finished' || $state === 'all')
                <h2><o>C</o>ommandes terminées :</h2>
            @endif
            @foreach ($baskets as $basket)
                @if ($basket->state == 'finished')
                    <p>Panier de {{ $basket->user[0]->name }}</p>
                    <p>Email : {{ $basket->user[0]->email }}</p>
                    <div class="headTable">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Brand</th>
                                    <th>Reference</th>
                                    <th>Quantité</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="bodyTable">
                        <table cellpadding="0" cellspacing="0">
                            <tbody>
                                @foreach ( $basket->products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>
                                            @foreach ($categories as $category)
                                                @if ($product->id_category === $category->id)
                                                    {{$category->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$product->pivot->quantity}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <p>Commande récupéré le {{$basket->dateRecupProduct}} à {{$basket->hourRecupProduct}}</p>
                    <br>
                    <br>
                @endif
            @endforeach
        </div>
    </div>
</div>
@include('components.perso.footer')
