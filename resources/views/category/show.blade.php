@vite(['resources/scss/app.scss', 'resources/js/app.js'])


@include('components.perso.header')

<div class="bodyProduct">
    <div class="categories">
        <i></i>
        <h2 class="titleListCategory"><o>C</o>atégories</h2>
        <div class="category">
            <a href="{{ route('product.index') }}">¤ Tous les Produits</a>
            @foreach ($categories as $category)
                <a href="{{ route('category.show', ['category'=>$category]) }}">¤ {{ $category->name }}</a>
            @endforeach
        </div>
        <u></u>
    </div>
    <div class="products">
        <h2><o>P</o>roduits</h2>
        <div class="headTable">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Brand</th>
                        <th>Reference</th>
                        <th>Stock</th>
                        <th>Panier</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="bodyTable">
            <table cellpadding="0" cellspacing="0">
                <tbody>
                    @foreach ( $products as $product)
                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->brand }}</td>
                            <td>
                                @foreach ($categories as $category)
                                    @if ($product->id_category === $category->id)
                                        {{$category->name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{$product->previsionalStock}}</td>
                            <td>
                                <form action="{{ route('product.addProduct', ['product'=>$product]) }}" method="POST">
                                    @csrf
                                    <input type="number" name="quantity" max="{{ $product->previsionalStock }}" min="1">
                                    <button type="submit" class="btn btn-primary">Ajouter</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('components.perso.footer')
