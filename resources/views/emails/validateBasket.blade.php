<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Votre Commande a été validée !</h1>

    <p>Recap produits :</p>
    @foreach ($products as $product)
        <li>{{$product->name}}</li>
    @endforeach
    <p>Commande prévu pour le {{$basket->dateRecupProduct}} à {{$basket->hourRecupProduct}}</p>
    <p>Cordialement.
        <br>
        Asperience Gives.
    </p>

</body>
</html>
