<div class="header">
    <div class="profil">
        <input type='image' class='imgProfil' src="images/profil.png" alt="icone profil"/>
        @if (auth()->check())
            <p class="nameUser">{{Auth::user()->name}}</p>
            <form action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit">Déconnexion</button>
            </form>
        @else
            <a href="{{ route('login') }}"><button>Connexion</button></a>
            <a href="{{ route('register') }}"><button>Inscription</button></a>
        @endif
    </div>

    <a href="{{ route('product.index') }}"><h1 class="titlePage"><o>Asp</o>rienceGives</h1></a>
    @if (auth()->check())
        @if (Auth::user()->is_admin === 1)
            <div class="addProduct">
                <a href="{{ route('admin.product.create') }}"><button>Ajouter un produit</button></a>
            </div>
        @else

        @endif
    @else

    @endif
    <div class="iconePanier">
        @auth
        <div class="historique">
            @if (auth()->check())
                @if (Auth::user()->is_admin === 1)
                    <a href="{{ route('admin.basket.historicBasketAdmin') }}">Commandes</a>
                @else
                    <a href="{{ route('basket.historic') }}">Mes Commandes</a>
                @endif
            @else
                <a href="{{ route('basket.historic') }}">Mes Commandes</a>
            @endif
        </div>
        <div class="panier">
            @if (auth()->check())
                @if (Auth::user()->is_admin === 1)

                @else
                    <a href="{{ route('basket.show', ['basket'=>$basket]) }}">
                        <input type='image' class='imgPanier' src="../public/images/panier.png" alt="image du panier"/>
                    </a>
                    <p class="countPanier">{{ $nbProductBasket }}</p>
                @endif
            @else
                <a href="{{ route('basket.show', ['basket'=>$basket]) }}">
                    <input type='image' class='imgPanier' src="./images/panier.png" alt="image du panier"/>
                </a>
                <p class="countPanier">{{ $nbProductBasket }}</p>
            @endif
        </div>
        @endauth
    </div>
</div>
