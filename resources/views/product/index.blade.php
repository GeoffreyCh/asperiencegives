@vite(['resources/scss/app.scss', 'resources/js/app.js'])

@include('components.perso.header')
<div class="bodyProduct">
    <div class="categories">
        <h2 class="titleListCategory"><o>C</o>atégories</h2>
        <div class="category">
            <a href="{{ route('product.index') }}">¤ Tous les Produits</a>
            @foreach ($categories as $category)
                <a href="{{ route('category.show', ['category'=>$category]) }}">¤ {{ $category->name }}</a>
            @endforeach
        </div>
    </div>
    <div class="products">
        <div class="titleProduct">
            <h2><o>P</o>roduits</h2>
        </div>
        <div class="headTable">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Marque</th>
                        <th>Catégorie</th>
                        <th>Qté</th>
                        @if (auth()->check())
                            @if (Auth::user()->is_admin === 1)
                                <th>Modifier</th>
                            @else
                                <th>Panier</th>
                            @endif
                        @else
                            <th>Panier</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
        <div class="bodyTable">
            <table cellpadding="0" cellspacing="0">
                <tbody>
                    @foreach ( $products as $product)
                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->brand }}</td>
                            <td>
                                @foreach ($categories as $category)
                                    @if ($product->id_category === $category->id)
                                        {{$category->name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{$product->previsionalStock}}</td>
                            <td>
                                @if (auth()->check())
                                    @if (Auth::user()->is_admin === 1)
                                        <div class="modifProduct">
                                            <a href="{{ route('admin.product.edit', ['product'=>$product]) }}"><button>🖉</button></a>
                                            <form action="{{ route('product.destroy', ['product'=>$product]) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit">🗑</button>
                                            </form>
                                        </div>
                                    @else
                                        <form action="{{ route('product.addProduct', ['product'=>$product]) }}" method="POST">
                                            @csrf
                                            <input type="number" name="quantity" max="{{ $product->previsionalStock }}" min="1">
                                            <button type="submit" class="btn btn-primary"><img src="./images/panier.png" alt="image panier"></button>
                                        </form>
                                    @endif
                                @else
                                    <form action="{{ route('product.addProduct', ['product'=>$product]) }}" method="POST">
                                        @csrf
                                        <input type="number" name="quantity" max="{{ $product->previsionalStock }}" min="1">
                                        <button type="submit" class="btn btn-primary"><img src="./images/panier.png" alt="image panier"></button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                        {{-- <a href="{{ route('product.show', ['product'=>$product]) }}"><button class="btn btn-danger">Voir detail produit</button></a> --}}
                        {{-- <div class="updateProduct">
                            <a href="{{ route('product.edit', ['product'=>$product]) }}"><button class="btn btn-primary">Modifier produit</button></a>
                        </div> --}}
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('components.perso.footer')
