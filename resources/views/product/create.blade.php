@vite(['resources/scss/app.scss', 'resources/js/app.js'])

@include('components.perso.header')
<br>
<div class="pageAddProduct">
    <div class="row">
        <h2>Importer vos données via formulaire</h2>
        <form class="col-8 d-flex flex-column justify-content-center" method="POST"  enctype="multipart/form-data" action="{{ route('admin.product.store') }}">
            @csrf
                <label for="name" class="form-label">Nom du produit</label>
                <input type="text" class="form-control" name="name">

                <label for="brand" class="form-label">Marque du produit</label>
                <input type="text" class="form-control" name="brand">

                <label for="reference" class="form-label">Reference</label>
                <input type="text" class="form-control" name="reference" maxlength="8">

                <label for="stock" class="form-label">Stock</label>
                <input type="number" class="form-control" name="stock">

                <label for="filepath" class="form-label">Image du produit</label>
                <input type="file" class="form-control" name="filepath">

                <label for="description" class="form-label">Description du produit</label>
                <textarea class="form-control" rows="5" name="description"></textarea>

                <label for="id_category" class="form-label">Catégorie</label>
                <select class="form-select" aria-label="Default select example" name="id_category">
                    <option selected>Selectionner une catégorie</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                <br>
                <div class="">
                    <button type="submit" class="btn btn-dark">Ajouter</button>
                </div>

        </form>
    </div>
    <div class="row">
        <h2>Importer vos données via csv</h2>
        <form class="col-8 d-flex flex-column justify-content-center" method="POST"  enctype="multipart/form-data" action="{{ route('admin.product.storeViaCsv') }}">
            @csrf
                <label for="file" class="form-label">Fichier Csv à importer</label>
                <input id="file" type="file" class="form-control" name="file" required>

                <br>
                <div class="">
                    <button type="submit" class="btn btn-dark">Ajouter</button>
                </div>

        </form>
    </div>
</div>
<br>
@include('components.perso.footer')



