@vite(['resources/scss/app.scss', 'resources/js/app.js'])

@include('components.perso.header')


<div class="row">
    <div class="col-2"></div>
    <form class="col-8 d-flex flex-column justify-content-center" method="POST" action="{{ route('admin.product.update', ['product'=>$product]) }}">
        @csrf
        @method('PUT')
            <label for="name" class="form-label">Nom du produit</label>
            <input type="text" class="form-control" name="name" value="{{ $product->name }}">

            <label for="brand" class="form-label">Marque du produit</label>
            <input type="text" class="form-control" name="brand" value="{{ $product->brand }}">

            <label for="reference" class="form-label">Reference</label>
            <input type="text" class="form-control" name="reference" maxlength="8" value="{{ $product->reference }}">

            <label for="stock" class="form-label">Stock</label>
            <input type="number" class="form-control" name="stock" value="{{ $product->stock }}">

            <label for="filepath" class="form-label">Image du produit</label>
            <input type="file" class="form-control" name="filepath" value="{{ $product->image }}>

            <label for="description" class="form-label">Description du produit</label>
            <textarea class="form-control" rows="5" name="description">{{ $product->description }}</textarea>

            <label for="id_category" class="form-label">Catégorie</label>
            <select class="form-select" aria-label="Default select example" name="id_category">
                <option>Selectionner une catégorie</option>
                @foreach($categories as $category)
                    @if ($product->id_category === $category->id)
                        <option value="{{$category->id}}" selected>{{$category->name}}</option>
                    @endif
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>

            <br>
            <div class="">
                <button type="submit" class="btn btn-dark">Modifier</button>
            </div>

    </form>
</div>

